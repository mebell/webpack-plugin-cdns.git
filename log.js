const log = {}

log.red = (msg) => {
  console.log('%s', '\033[31m ' + msg + ' \033[0m')
}

log.green = (msg) => {
  console.log('%s', '\033[32m ' + msg + ' \033[0m')
}

log.blue = (msg) => {
  console.log('%s', '\033[34m ' + msg + ' \033[0m')
}

log.yellow = (msg) => {
  console.log('%s', '\033[33m ' + msg + ' \033[0m')
}

log.popurse = (msg) => {
  console.log('%s', '\033[35m ' + msg + ' \033[0m')
} 

module.exports = log