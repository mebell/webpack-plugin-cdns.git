# webpack-plugin-cdns

## 概述 Summary

Webpack 插件

支持版本 3,4,5

将打包完成后的文件上传到 CDN (阿里云 OSS, 亚马逊 S3, 七牛云 Kodo)


Webpack Plugin

Supports versions 3,4,5

Upload the packaged file to the CDN (Ali Cloud OSS, Amazon S3, Qiniu Cloud Kodo)

## 用法 Usage

### 安装 install
```
npm install webpack-plugin-cdns
```

### 引入 import
```
const WebpackPluginCdns = require('webpack-plugin-cdns')
```

### 使用 use

`Ali Cloud OSS`
```
plugins: [
  ...,
  new WebpackPluginCdns({
    from: ['./dist/**', '!*.html'],
    dist: '/directory/',
    // Ali Cloud OSS (options: https://help.aliyun.com/document_detail/64097.html)
    oss: {
      accessKeyId: '',
      accessKeySecret: '',
      bucket: '',
      region: '',
    }
  })
]
```

`Amazon S3`
```
plugins: [
  ...,
  new WebpackPluginCdns({
    from: ['./dist/**', '!*.html'],
    dist: '/directory/',
    // Amazon S3
    s3: {
      accessKeyId: '',
      secretAccessKey: '',
      Bucket: '',
      region: ''
    }
  })
]
```

`Qiniu Cloud Kodo`
```
plugins: [
  ...,
  new WebpackPluginCdns({
    from: ['./dist/**', '!*.html'],
    dist: '/directory/',
    // 七牛云 Kodo
    kodo: {
      accessKey: '',
      secretKey: '',
      bucket: ''
    }
  })
]
```

### Options 

#### from and dist（required）

from:

需要上传的文件; 详细书写规则请查看globby

Files to be uploaded; See globby for detailed writing rules


dist:

CDN 资源存放目录

CDN Resource Storage Directory

#### oss,s3,kodo (optional)

选择其中一项配置即可；当同时配置多个，则只会上传到第一个匹配的cdn仓库，匹配顺序：oss>s3>kodo

Select one of the configurations. If multiple cdn files are configured at the same time, only the first matching CDN repository is uploaded. The matching sequence is oss>s3>kodo
